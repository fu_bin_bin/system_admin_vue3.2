// 国际化
import { createI18n } from "vue-i18n";

// 数据源
const messages = {
    en: {
        msg: {
            test: 'one'
        }
    },
    zh: {
        msg: {
            test: '一'
        }
    }
}
const locale = 'cn'

// 初始化i18n实例
const i18n = createI18n({
    legacy: false, // 使用Composition API描述，则需要设置为false
    globalInjection: true, // 全局注入 $t 函数
    locale,
    messages
})

export default i18n