// 接口集合
import request from '@/utils/request'

/**
 * 登录
 */
 export const login = data => {
   return request({
     url: '/sys/login',
     method: 'POST',
     data
   })
 }
/**
* 获取用户
*/
export const getUserInfo = () => {
  return request({
    url: '/sys/profile'
  })
}