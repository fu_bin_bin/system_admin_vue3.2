/**
 *封装配置环境变量封装 axios 模块
 */
import axios from 'axios'
import { ElMessage } from 'element-plus'
import store from '@/store'
import { isCheckTimeOut } from '@/utils/auth'

const service = axios.create({
  baseURL : process.env.VUE_APP_BASE_API,
  timeout : 5000
})

// 请求拦截器
service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      // 判断是否超时
      if (isCheckTimeOut()) {
        // 退出
        store.dispatch('user/logout')
        return Promise.reject(new Error('登录超时！ '))
      }
      // 如果token存在 注入token
      config.headers.Authorization = `Bearer ${store.getters.token}`
    }
    return config
  },error => {
    return Promise.reject(error)
  }
)


// 响应拦截器
service.interceptors.response.use(
  response => {
    const {success, message, data} = response.data 
    if (success) {
      ElMessage({
        message,
        type: 'success',
      })
      return data
    } else {
      ElMessage.error(message)
      return Promise.reject(new Error(message))
    }
  },
  error => {
    // 处理 token 超时问题
    if (error.response && error.response.data && error.response.data.code === 401) {
      // token 超时
      store.dispatch('user/logout')
    }
    ElMessage.error(error.message)
    return Promise.reject(error)
  }
)
export default service