import path from 'path'

export const generateRoutes = (routes, basePath = '/', prefixTitle = []) => {
  // 创建result 数据
  let res = []
  // 循环routes 路由
  for (const route of routes) {
    // 创建包含path和title的item
    const data = {
      path: path.resolve(basePath, route.path),
      title: [...prefixTitle]
    }
    // 当前存在meta时
    // 动态路由不允许被搜索
    // 匹配动态路由的正则
    const re = /.*\/:.*/

    if (route.meta && route.meta.title && !re.exec(route.path)) {
      data.title = [...data.title,route.meta.title]
      res.push(data)
    }
    // 存在children时，迭代调用
    if (route.children) {
      const tempRoutes = generateRoutes(route.children, data.path, data.title)
      if (tempRoutes.length >= 1) {
        res = [...res, ...tempRoutes]
      }
    }
  }
  return res
}