/**
 * 公开路由表
 */
 import layout from '@/pages/layout/index'

export default [
    {   
      path: '/login',
      component: () => import('@/pages/login/index')
    },
    {
      path: '/',
      component: () => layout,
      redirect: '/profile',
      children: [
        {
          path: '/profile',
          name: 'profile',
          component: () => import('@/pages/profile/index'),
          meta: {
            title: '个人中心',
            icon: 'profile'
          }
        },
        {
          path: '/404',
          name: '404',
          component: () => import('@/pages/error-page/404')
        },
        {
          path: '/401',
          name: '401',
          component: () => import('@/pages/error-page/401')
        }
      ]
    }
  ]