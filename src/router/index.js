import { createRouter, createWebHashHistory } from 'vue-router'
import privateRoutes from './privateRoutes' // 私有的路由表
import publicRoutes from './publicRoutes' // 公开的路由表



const router = createRouter({
  history: createWebHashHistory(),
  routes: [...publicRoutes,...privateRoutes,  {
          path: "/:pathMatch(.*)*", //不符合的路由跳转个人中心
          redirect: '/profile'
        }]
})

export default router