import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/styles/index.scss' //样式初始化
import i18n from '@/i18n' // 导入初始化后的i18n


// import installElementPlus from ''
// import installElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

// import 'element-plus/theme-chalk/index.css'

import './permission' // 导入路由鉴权
import installIcons from '@/icons' // 导入svgIcon

const app = createApp(App)
app.use(store)
app.use(router)
app.use(ElementPlus)

app.use(installIcons)
app.use(i18n)
app.mount('#app')