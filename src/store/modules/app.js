import { getItem,setItem } from '@/utils/storage'
import { TAGS_VIEW } from '@/constant'

export default {
    namespaced: true, // 表示单独的模块
    state: () => ({
        sidebarOpened: true, // Sidebar动画
        tagsViewList: getItem(TAGS_VIEW) || []
    }),
    mutations: {
        // sidebar 设置左边伸展
        triggerSidebarOpened(state) {
            state.sidebarOpened = !state.sidebarOpened
        },
        //添加 tags
        addTagsViewList(state, tag) {
            const isFind = state.tagsViewList.find(item => {
                return item.path === tag.path
              })
            // 处理重复
            if (!isFind) {
                state.tagsViewList.push(tag)
                setItem(TAGS_VIEW, state.tagsViewList)
            }
        },
        // 删除 tag
        removeTagsView(state, payload) {
            if (payload.type === 'index') {
                state.tagsViewList.splice(payload.index, 1)
            } else if (payload.type === 'other') {
                state.tagsViewList.splice(
                payload.index + 1,
                state.tagsViewList.length - payload.index + 1
                )
                state.tagsViewList.splice(0, payload.index)
            } else if (payload.type === 'right') {
                state.tagsViewList.splice(
                payload.index + 1,
                state.tagsViewList.length - payload.index + 1
                )
            }
            setItem(TAGS_VIEW, state.tagsViewList)
        },
    },
    actions: {}
}