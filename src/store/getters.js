// 快捷方式
import variables from '@/styles/variables.scss'
import store from '@/store'
import { generateColors } from '@/utils/theme.js'
import { getItem } from '@/utils/storage'
import { MAIN_COLOR } from '@/constant'


export default {
    token : state => state.user.token, 
    hasUserInfo : state => {
       return JSON.stringify(state.user.userInfo) !== '{}'
    },
    userInfo: state => state.user.userInfo, // 用户信息
    cssVar: state => ({
        ...state.theme.variables,
        ...generateColors(getItem(MAIN_COLOR))
    }), // 全局scss
    sidebarOpened: state =>  state.app.sidebarOpened, //Sidebar 状态
    language: state => state.app.language, //获取当前语言
    mainColor: state => state.theme.mainColor , //获取当主题颜色
    tagsViewList: state => state.app.tagsViewList //获取当前选择过的路由list
}