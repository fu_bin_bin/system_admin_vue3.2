// 路由鉴权
import router from '@/router'
import store from '@/store'
/**
 * 全局前置守卫
 * @param {*} to 要到去哪里
 * @param {*} from 你从哪里来
 * @param {*} next 是否去？
 */
// 白名单
const whiteList = ['/login']
router.beforeEach( async (to, from, next) => { 
  if (store.getters.token) {
    // 1.用户已登录，不允许进login
    if (to.path === '/login') {
      next('/')
    } else {
      next()
      // 判断用户信息是否存在
      if (!store.getters.hasUserInfo) {
        // 获取用户信息
        await store.dispatch('user/getUserInfo')
      }
    }
  } else {
    // 2.用户未登录，只允许进login
    // indexOf() 如果要检索的字符串值没有出现，则该方法返回 -1
    if (whiteList.indexOf(to.path) > -1) {
      next()
    } else {
      next('/login')
    }
  }


})