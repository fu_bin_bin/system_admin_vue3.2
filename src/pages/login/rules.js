/**
 * 登录校验规则
 */

// 用户
const username_rules = [{
  required: true,
  message: 'Please input Activity name',
  trigger: 'blur'
}]

let validatePassword = function() {
  return (rule, value, callback) => {
    // let phoneTest = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,18}$/

    // if (!phoneTest.test(value)) {
    //     callback(new Error('密码须包含数字、字母两种元素，且密码位数为6-16位'))
    // }else {
    //   callback()
    // }

    if (value.length < 6){
      callback(new Error('不能小于6位数'))
    }else{
      callback()
    }
  }
}
// 密码
const password_rules = [{
  required: true,
  trigger: 'blur',
  validator : validatePassword()
}]

 export {
  username_rules,
  password_rules
 }